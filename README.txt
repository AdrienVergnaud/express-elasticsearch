INSTALL:
  /home/ubuntu/dev/bitbucket/express-elasticsearch>
  npm init
  npm install express --save
  npm install elasticsearch --save
  npm install --save body-parser
  npm install --save express-hateoas-links

Base elasticsearch :
  index : meetup
  type : personne

RUN
  /home/ubuntu/dev/elasticsearch-5.2.0>./bin/elasticsearch
  node server.js

TEST :
  curl -X POST -H "Content-Type:application/json" -d '{ "prenom" : "Jean", "om" : "Dupont" }' http://localhost:3000/api/personnes/

  Retourne :
    {
      "_index": "meetup",
      "_type": "personne",
      "_id": "AVosmNbb4DBaEtKJ8twR",
      "_version": 1,
      "result": "created",
      "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
      },
      "created": true,
      "links": [
        {
          "rel": "self",
          "method": "GET",
          "href": "http://localhost:3000/api/personnes/AVosmNbb4DBaEtKJ8twR"
        },
        {
          "rel": "personne",
          "method": "GET",
          "href": "http://localhost:3000/api/personnes/AVosmNbb4DBaEtKJ8twR"
        }
      ]
    }
